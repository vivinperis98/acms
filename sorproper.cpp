#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	ofstream myfile;
  	myfile.open ("result123.csv");
	int a[4][4]={{10,-1,2,0},
				{-1,11,-1,3},
				{2,-1,10,-1},
				{0,3,-1,8}};
	auto start = chrono::high_resolution_clock::now(); 
	for(double w=0.01;w<=1.99;w=w+0.01)
	{
	int c[4]={6,25,-11,15};
	int co=1,n=4;
	double e=1.0;
	double x[4][2]={0};
	while(e>0.0001)
	{
		double err=0;
		for(int i=0;i<n;i++)
		{
			double sum=0,sumx=0;
			for(int j=0;j<n;j++)
			{
				if(i!=j)
				{
					sumx+=x[j][(co+1)%2];
					if(j<i)
					{
						sum+=a[i][j]*x[j][(co)%2];
					}
					else
					{
						sum+=a[i][j]*x[j][(co+1)%2];
					}	
				}
			}
		x[i][co%2]=(w*(c[i]-sum)/a[i][i])+(1-w)*x[i][(co+1)%2];
		if(i==0)
		{
			err=abs(x[i][co%2]-x[i][(co+1)%2])/x[i][co%2];
		}
	    else if(abs((x[i][co%2]-x[i][(co+1)%2])/x[i][co%2])>err)
		{
			err=abs((x[i][co%2]-x[i][(co+1)%2])/x[i][co%2]);
		}
		}
		co++;
		e=err;
	}
	cout<<"No of iterations"<<co-1<<"\n"<<"w="<<w<<"\n";
	myfile << w<<","<<(co-1)<<"\n";
	for(int i=0;i<n;i++)
	{
		cout<<"x["<<i+1<<"]="<<x[i][(co-1)%n]<<"\n";
	}
	}
	myfile.close();
	auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl; 
	return 0;
}