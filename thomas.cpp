#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
using namespace std;
int main()
{
	double arr[4][4]={{2.08,-1,0,0},
				   {-1,2.08,-1,0},
				   {0,-1,2.08,-1},
				   {0,0,-1,2.08}};
	auto start = chrono::high_resolution_clock::now(); 
	double out[4]={41.6,1.6,1.6,201.6};
	double sol[4]={0};
	int n=4;
	for(int j=1;j<n;j++)
	{
		double rat=(arr[j-1][j]/arr[j-1][j-1]);
		arr[j][j]=arr[j][j]-(rat*arr[j][j-1]);
		out[j]=out[j]-rat*out[j-1];
	}
	for(int i=0;i<n;i++)
	{
		for(int j=0;j<n;j++)
		{
			cout<<arr[i][j]<<"\t";
		}
		cout<<"|"<<out[i];
		cout<<"\n";
	}
	sol[n-1]=out[n-1]/arr[n-1][n-1];
	for(int j=n-2;j>=0;j--)
	{
		sol[j]=(out[j]-arr[j][j+1]*sol[j+1])/arr[j][j];
	}
	cout<<"------------------\nSolution\n------------------\n";
	for(int i=0;i<n;i++)
	{
		cout<<"T"<<i<<"="<<sol[i]<<"\n";
	}
	auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl; 
}