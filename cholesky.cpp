#include<bits/stdc++.h>
#include<math.h>
using namespace std;
int main()
{
    double arr[5][5]={{2,1,1,3,2},
                    {1,2,2,1,1},
                     {1,2,9,1,5},
                     {3,1,1,7,1},
                     {2,1,5,1,8}};
    int n=5;
    double upper[n][n],lower[n][n];
    memset(lower, 0, sizeof(lower)); 
    memset(upper, 0, sizeof(upper)); 
    for(int i=0;i<n;i++)
    {
        double sum=0;
        for(int k=0;k<i;k++)
        {
            sum+=upper[k][i]*upper[k][i];
        }
        upper[i][i]=sqrt(arr[i][i]-sum);
        for(int j=i+1;j<n;j++)
        {
            double sum2=0;
            for(int k=0;k<i;k++)
            {
                sum2+=upper[k][i]*upper[k][j];
            }
            upper[i][j]=(arr[i][j]-sum2)/upper[i][i];
        }
    }
    cout << "\nUpper Triangular\n----------------------------------------------\n";
    for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++) 
            cout << upper[i][j] << "\t"; 
        cout << endl; 
    }
}