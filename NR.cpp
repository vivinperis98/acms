#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now();
	double ig=2.0;//Since the value lies between 0 and 6
	double err=1;
	int count=0;
	while(err>0.00001)
	{
		double temp=ig;
		ig=ig-(9*M_PI*pow(ig,2)-M_PI*pow(ig,3)-90)/(18*M_PI*ig-3*M_PI*pow(ig,2));
		count++;
		cout<<"Iteration "<<count<<"="<<ig<<"\n";
		err=abs(temp-ig);
		cout<<"Relative Error="<<err/ig*100<<"%\n";
	}
	cout<<"Number of iterations"<<count<<"\n";
	auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl;
}