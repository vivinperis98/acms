#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now();
	double angle0=M_PI/4;
	double angle,e;
	int c=0;
	do
	{
	double df = 4*cos(angle0) + 4*cos(2*angle0);
 	double d2f = -4*sin(angle0) - 8*sin(2*angle0);
 	angle = angle0 - df/d2f;
 	e=abs(angle-angle0);
 	angle0=angle;
 	c++;
 }while(e>=0.05);
 cout<<"Angle ="<<angle<<" Radians"<<"="<<angle*180/M_PI<<" degree";
 cout<<"\nNumber of iterations="<<c;
 cout<<"\nArea max="<<4.0*sin(angle)*(1+cos(angle));
 auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl;
}