#include<bits/stdc++.h>
using namespace std;
int main()
{
	double arr[5][5]={{2,1,1,3,2},
					{1,2,2,1,1},
					 {1,2,9,1,5},
					 {3,1,1,7,1},
					 {2,1,5,1,8}};
	int n=5;
	double upper[n][n],lower[n][n];
	memset(lower, 0, sizeof(lower)); 
    memset(upper, 0, sizeof(upper)); 
    cout<<"=========================================\nEnter\n1)Doolittle Algorithm\n2)Crout's Algorithm\n3)Cholesky Decomposition\n";
    int choice=0;
    cin>>choice;
    if(choice==1)
    {
	for (int i = 0; i < n; i++) 
	{ 
       for (int k = i; k < n; k++) { 
  
            double sum = 0; 
            for (int j = 0; j < i; j++) 
                sum += (lower[i][j] * upper[j][k]); 
            upper[i][k] = arr[i][k] - sum; 
        } 
        for (int k = i; k < n; k++) { 
            if (i == k) 
                lower[i][i] = 1;  
            else { 
                double sum = 0; 
                for (int j = 0; j < i; j++) 
                    sum += (lower[k][j] * upper[j][i]); 
                lower[k][i] = (arr[k][i] - sum) / upper[i][i]; 
            } 
        } 
    }
    }
    else if(choice==2)
    {
    	for (int i = 0; i < n; i++) 
    	{ 
       upper[i][i]=1;
       for(int j=0;j<=i;j++)
       {
            double sum=0;
            for(int k=0;k<j;k++)
            {
                sum=sum+lower[i][k]*upper[k][j];
            }
            lower[i][j]=arr[i][j]-sum;
       }
       for(int j=i+1;j<n;j++)
       {
        double sum=0;
            for(int k=0;k<i;k++)
            {
                sum+=lower[i][k]*upper[k][j];
            }
            upper[i][j]=(arr[i][j]-sum)/lower[i][i];
       	}
    } 
    }
    else if(choice==3)
    {
    	for(int i=0;i<n;i++)
    {
        double sum=0;
        for(int k=0;k<i;k++)
        {
            sum+=upper[k][i]*upper[k][i];
        }
        upper[i][i]=sqrt(arr[i][i]-sum);
        for(int j=i+1;j<n;j++)
        {
            double sum2=0;
            for(int k=0;k<i;k++)
            {
                sum2+=upper[k][i]*upper[k][j];
            }
            upper[i][j]=(arr[i][j]-sum2)/upper[i][i];
        }
    }
    } 
    cout << "Lower Triangular\n----------------------------------------------\n";
    for (int i = 0; i < n; i++) 
    { 
        for (int j = 0; j < n; j++) 
            cout <<lower[i][j] << "\t";  
        cout << "\n";
    } cout << "\nUpper Triangular\n----------------------------------------------\n";
    for (int i = 0; i < n; i++) { 
    for (int j = 0; j < n; j++) 
            cout << upper[i][j] << "\t"; 
        cout << endl; 
    }
}