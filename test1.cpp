#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now();
	double xl=0.5,xu=2.5;
	int count=0;
	double mean[2];
	double e=1;
	do
	{
		cout<<"Iteration "<<count+1<<"\n";
		mean[count%2]=(xl+xu)/2;	
		double fxl=1-(20*20)*(3+xl)/(9.81*pow(3*xl+(pow(xl,2)/2),3));
		double fxu=1-(20*20)*(3+xu)/(9.81*pow(3*xu+(pow(xu,2)/2),3));
		cout<<"F(xl)="<<fxl<<"\n";
		cout<<"F(xu)="<<fxu<<"\n";
		double fmean=1-(20*20)*(3+mean[count%2])/(9.81*pow(3*mean[count%2]+(pow(mean[count%2],2)/2),3));
		cout<<"Xmean="<<mean[count%2]<<"\n";
		cout<<"F(xmean)"<<fmean<<"\n";
		cout<<"F(xl)*fmean="<<fxl*fmean;
		if(fxl*fmean<0)
        {
            xu=mean[count%2];
        }
        else
        {
            xl=mean[count%2];
        }
        if(count!=0)
        {
        	e=abs((mean[count%2]-mean[(count+1)%2]))/mean[count%2];
        	cout<<"\nError="<<e<<"\n\n";
        }
        else 
        {
        	cout<<"\n\n";
        }
        count++;
	}while(e>=0.001);
	cout<<"Root estimate="<<mean[(count+1)%2];
	 auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl;
}