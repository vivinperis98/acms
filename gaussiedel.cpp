#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now(); 
	ofstream myfile;
  	myfile.open ("resultgausssiedel.csv");
	int a[4][4]={{10,-1,2,0},
				{-1,11,-1,3},
				{2,-1,10,-1},
				{0,3,-1,8}};
	int c[4]={6,25,-11,15};
	int co=1,n=4;
	double e=1.0;
	double x[4][2]={0};
	while(e>0.0001)
	{
		double err=0;
		for(int i=0;i<n;i++)
		{
			double sum=0;
			for(int j=0;j<n;j++)
			{
				if(i!=j)
				{
					if(j<i)
					{
						sum+=a[i][j]*x[j][(co)%2];
					}
					else
					{
						sum+=a[i][j]*x[j][(co+1)%2];
					}	
				}

			}
		x[i][co%2]=(c[i]-sum)/a[i][i];
		if(i==0)
		{
			err=abs(x[i][co%2]-x[i][(co+1)%2])/x[i][co%2];
		}
	    else if(abs((x[i][co%2]-x[i][(co+1)%2])/x[i][co%2])>err)
		{
			err=abs((x[i][co%2]-x[i][(co+1)%2])/x[i][co%2]);
		}
		myfile<<x[i][co%2];
		if(i!=n-1)
		{
			myfile<<",";
		}
		}
		myfile<<"\n";
		co++;
		e=err;
	}
	cout<<co-1<<"\n";
	for(int i=0;i<n;i++)
	{
		cout<<"x["<<i+1<<"]="<<x[i][(co-1)%2]<<"\n";
	}
	myfile.close();
	auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl; 
}