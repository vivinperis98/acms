#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now();
	double rat=(sqrt(5)-1)/2;
	double xu=M_PI/2,xl=0;
	int c=0;
	do
	{
		c++;
		double x1=xl+rat*abs(xu-xl);
		double x2=xu-rat*abs(xu-xl);
		if((4*sin(x1)*(1+cos(x1)))>(4*sin(x2)*(1+cos(x2))))
		{
			xl=x2;
		}
		else
		{
			xu=x1;
		}
		cout<<"iteration "<<c<<" = "<< (xu+xl)/2<<"\n";
	}while(abs(xl-xu)>=0.05);
	double angle=(xu+xl)/2;
	 cout<<"Angle ="<<angle<<" Radians"<<"="<<angle*180/M_PI<<" degree";
 	cout<<"\nNumber of iterations="<<c;
 	cout<<"\nArea max="<<4.0*sin(angle)*(1+cos(angle));
auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl;
}

