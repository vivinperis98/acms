#include<bits/stdc++.h>
#include<math.h>
#include<chrono>
#include<fstream>
#include<iostream>
using namespace std;
int main()
{
	auto start = chrono::high_resolution_clock::now();
	double h=0.5; //Initial guess value
	double E=100;
	int count=0;
	while(E>0.05)
	{
		double temp=h;
		h=0.20621*pow((20+2*h),0.4); 
		count++;
		cout<<"Iteration"<<count<<":"<<h<<"\n";
		E=abs(h-temp)/h*100;
		cout<<"Error"<<count<<":"<<E<<"%\n";
	}
	cout<<"Final value="<<h<<"\nNo of iterations="<<count;
	auto end = chrono::high_resolution_clock::now(); 
    double time_taken =  
      chrono::duration_cast<chrono::nanoseconds>(end - start).count();
    time_taken *= 1e-9; 
    cout << "\nTime taken by program is : " << fixed  
         << time_taken << setprecision(9); 
    cout << "sec" << endl;
}